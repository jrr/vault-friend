/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package v1alpha2

import (
	"io/ioutil"
	"strings"

	"github.com/pkg/errors"

	vaultApi "github.com/hashicorp/vault/api"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	tokenCache = make(map[string]string)
)

// DataSourceSpec defines the desired state of a DataSource
// +kubebuilder:validation:Required
type DataSourceSpec struct {
	// HostURL is the fully-qualified URL of the Vault server. The format
	// of address should be "<Scheme>://<Host>:<Port>".
	// +kubebuilder:default="https://vault:8200"
	HostURL string `json:"hostURL"`

	// AuthEngineMountPath is the particular instance of the
	// Kubernetes auth engine in Vault to use, if you're running more
	// than one.
	// +kubebuilder:default="kubernetes"
	// +kubebuilder:validation:Optional
	AuthEngineMountPath string `json:"authEngineMountPath"`

	// MetadataRole is the role that Vault-Friend should use to
	// connect to the HostURL with its Kubernetes service account for
	// the purpose of reading metadata for update checking.
	// +kubebuilder:default="vault-friend"
	// +kubebuilder:validation:Optional
	MetadataRole string `json:"metadataRole,omitempty"`

	// TLSConfig contains the parameters needed to configure TLS on the HTTP client
	// used to communicate with Vault.
	// +kubebuilder:validation:Optional
	TLSConfig *TLSConfig `json:"tlsConfig,omitempty"`
}

func (dss *DataSourceSpec) CreateClient() (*vaultApi.Client, error) {
	var err error

	vaultConfig := vaultApi.DefaultConfig()

	if dss.TLSConfig != nil {
		err = vaultConfig.ConfigureTLS(dss.TLSConfig.VaultTLSConfig())

		if err != nil {
			return nil, errors.Wrap(err, "unable to configure Vault TLS settings")
		}
	}

	client, err := vaultApi.NewClient(vaultConfig)

	if err != nil {
		return nil, err
	}

	err = client.SetAddress(dss.HostURL)

	if err != nil {
		return nil, errors.Wrap(err, "unable to configure Vault URL")
	}

	return client, nil
}

func (dss *DataSourceSpec) AuthenticatedClient(role, token string) (*vaultApi.Client, error) {

	vaultClient, err := dss.CreateClient()
	if err != nil {
		return nil, errors.Wrap(err, "cannot create Vault client")
	}

	tokenValid, err := dss.CheckCachedToken(role)
	if err != nil {
		return nil, errors.Wrap(err, "error checking cached token")
	}

	if tokenValid {
		vaultClient.SetToken(tokenCache[role])
	} else {

		delete(tokenCache, role)

		authBody := map[string]string{
			"role": role,
			"jwt":  token,
		}

		authPathParts := []string{
			"",
			"v1",
			"auth",
			dss.AuthEngineMountPath,
			"login",
		}

		req := vaultClient.NewRequest("POST", strings.Join(authPathParts, "/"))

		if err = req.SetJSONBody(authBody); err != nil {
			return nil, errors.Wrap(err, "cannot set Vault auth JSON body")
		}

		resp, err := vaultClient.RawRequest(req)

		if err != nil {
			return nil, errors.Wrap(err, "cannot authenticate to Vault")
		}

		defer resp.Body.Close()

		rawSecret, err := vaultApi.ParseSecret(resp.Body)

		if err != nil {
			return nil, errors.Wrap(err, "cannot parse Vault response")
		}

		if rawSecret.Auth == nil {
			return nil, errors.New("no authentication information received from Vault")
		}

		tokenCache[role] = rawSecret.Auth.ClientToken
		vaultClient.SetToken(rawSecret.Auth.ClientToken)
	}

	return vaultClient, nil
}

func (dss *DataSourceSpec) CheckCachedToken(role string) (bool, error) {
	token, tokenOk := tokenCache[role]
	if !tokenOk {
		return false, nil
	}

	client, err := dss.CreateClient()
	if err != nil {
		return false, errors.Wrap(err, "error creating vault client")
	}

	client.SetToken(token)

	checkPathParts := []string{
		"",
		"v1",
		"auth",
		"token",
		"lookup-self",
	}

	req := client.NewRequest("GET", strings.Join(checkPathParts, "/"))

	resp, err := client.RawRequest(req)
	defer resp.Body.Close()

	if err != nil {
		return false, nil
	}

	return true, nil
}

func (dss *DataSourceSpec) MetadataClient() (*vaultApi.Client, error) {
	jwt, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/token")

	if err != nil {
		return nil, errors.Wrap(err, "cannot get serviceaccount token")
	}

	vaultClient, err := dss.AuthenticatedClient(dss.MetadataRole, string(jwt))

	if err != nil {
		return nil, errors.Wrap(err, "could not create authenticated Vault client")
	}

	return vaultClient, nil
}

// DataSourceRef allows use to have both cluster and namespace
// level data sources referenced with a single type.
// +kubebuilder:validation:Required
type DataSourceRef struct {
	// Kind is the kind of the DataSource to use,
	// either "DataSource" or "ClusterDataSource".
	// +kubebuilder:default=DataSource
	// +kubebuilder:validation:Enum=ClusterDataSource;DataSource
	Kind string `json:"kind"`

	// Name is the name of the DataSource to use.
	Name string `json:"name"`

	// AuthRole is the name of the role in the Vault Kubernetes auth
	// engine to use for authn/authz with this Environment.
	AuthRole string `json:"authRole"`
}

// +kubebuilder:validation:Optional
type TLSConfig struct {
	// CACert is the path to a PEM-encoded CA cert file to use to verify the
	// Vault server SSL certificate.
	CACert string `json:"caCert,omitempty"`

	// CAPath is the path to a directory of PEM-encoded CA cert files to verify
	// the Vault server SSL certificate.
	CAPath string `json:"caPath,omitempty"`

	// ClientCert is the path to the certificate for Vault communication
	ClientCert string `json:"clientCert,omitempty"`

	// ClientKey is the path to the private key for Vault communication
	ClientKey string `json:"clientKey,omitempty"`

	// TLSServerName, if set, is used to set the SNI host when connecting via TLS.
	TLSServerName string `json:"tlsServerName,omitempty"`

	// Insecure enables or disables SSL verification
	Insecure bool `json:"insecure,omitempty"`
}

func (tc *TLSConfig) VaultTLSConfig() *vaultApi.TLSConfig {
	return &vaultApi.TLSConfig{
		CACert:        tc.CACert,
		CAPath:        tc.CAPath,
		ClientCert:    tc.ClientCert,
		ClientKey:     tc.ClientKey,
		TLSServerName: tc.TLSServerName,
		Insecure:      tc.Insecure,
	}
}

// DataSourceStatus defines the observed state of DataSource
// +kubebuilder:validation:Optional
type DataSourceStatus struct {
	// Connected indicates whether the operator was able to
	// successfully connect to the HostURL.
	// +kubebuilder:default=false
	Connected bool `json:"connected"`

	// Unsealed indicates whether the target Vault is unsealed
	// and responding to requests for secrets.
	// +kubebuilder:default=false
	Unsealed bool `json:"unsealed"`

	// Message is a human-readable status indication.
	Message string `json:"message,omitempty"`

	// LastTouch is the last time the controller modified the object.
	LastTouch *metav1.Time `json:"lastTouch,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=dsrc

// DataSource contains the necessary information for
// connecting to a Vault server for fetching
// metadata and secret data.
// +kubebuilder:printcolumn:name="Connected",type=boolean,JSONPath=`.status.connected`,priority=0,description="Operator was able to connect to Vault"
// +kubebuilder:printcolumn:name="Unsealed",type=boolean,JSONPath=`.status.unsealed`,priority=10,description="Vault is ready to serve secrets"
type DataSource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DataSourceSpec   `json:"spec,omitempty"`
	Status DataSourceStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// DataSourceList contains a list of DataSource
type DataSourceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DataSource `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DataSource{}, &DataSourceList{})
}
