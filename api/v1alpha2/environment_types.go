/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package v1alpha2

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ForceRefreshKey = "force-refresh"
)

// EnvironmentSpec defines the desired state of an Environment
// +kubebuilder:validation:Required
type EnvironmentSpec struct {
	// SecretName is the name that will be given to the Kubernetes
	// Secret in which the Vault data is stored for application use.
	SecretName string `json:"secretName"`

	// SecretType is the `type` key given to Kubernetes when creating
	// the Secret in which in the Vault data is stored for application use.
	// +kubebuilder:default=Opaque
	// +kubebuilder:validation:Optional
	// +kubebuilder:validation:Enum=Opaque;kubernetes.io/service-account-token;kubernetes.io/dockercfg;kubernetes.io/dockerconfigjson;kubernetes.io/basic-auth;kubernetes.io/ssh-auth;kubernetes.io/tls;bootstrap.kubernetes.io/token
	SecretType string `json:"secretType"`

	// DataSourceRef is the Vault data source that should be used for
	// fetching meta- and secret data for this Environment.
	DataSourceRef *DataSourceRef `json:"dataSourceRef"`

	// ServiceAccountName is the Kubernetes service account whose token
	// should be used for connecting to the DataSource. Typically this
	// will be the same service account used by your application.
	ServiceAccountName string `json:"serviceAccountName"`

	// SourcePath specifies where the operator should look to find secret data.
	SourcePath *SourcePath `json:"sourcePath"`

	// AlwaysUpdate is a flag to disable version checking.
	// If false, the Vault metadata will be checked against
	// .status.version and only update the Secret if it differs.
	// +kubebuilder:default=false
	// +kubebuilder:validation:Optional
	AlwaysUpdate bool `json:"alwaysUpdate"`

	// PinVersion forces a particular version of a KV v2 path to be used.
	// If non-zero, AlwaysUpdate is ignored.
	// +kubebuilder:default=0
	// +kubebuilder:validation:Optional
	PinVersion int `json:"pinVersion"`

	// KeyPrefix adds a string prefix to each key in the path.
	// The KeyPrefix step happens BEFORE the CapitalizeKeys step.
	// +kubebuilder:validation:Optional
	KeyPrefix string `json:"keyPrefix,omitempty"`

	// CapitalizeKeys is a flag to transform all keys into ENV VAR STYLE.
	// The CapitalizeKeys step happens AFTER the KeyPrefix step.
	// +kubebuilder:default=false
	// +kubebuilder:validation:Optional
	CapitalizeKeys bool `json:"capitalizeKeys"`

	// CheckInterval determines how long the operator will wait
	// between checking on this object if there are no errors.
	// This must conform to standards for Golang's time.ParseDuration().
	// Notably, the largest unit is hours. If unset, default is 15 minutes.
	// +kubebuilder:validation:Optional
	CheckInterval string `json:"checkInterval,omitempty"`
}

// SourcePath is the interface to supporting more than just KV sources.
// When support for other secrets engines is added, the validation will
// change to requiring any one of the possible engines rather than requiring KV.
type SourcePath struct {
	// KV is the data path (e.g., "kv/data/foo") to be used as the source of the
	// synchronised secret.
	// +kubebuilder:validation:Required
	KV string `json:"kv,omitempty"`
}

// EnvironmentStatus defines the observed state of Environment
// +kubebuilder:validation:Optional
type EnvironmentStatus struct {
	// Version is the last fetched KV v2 secret version.
	// +kubebuilder:default=0
	Version int `json:"version"`

	// LastSync is the last time the environment data was pulled from Vault.
	LastSync *metav1.Time `json:"lastSuccess,omitempty"`

	// UpdateAvailable indicates whether the metadata version
	// is older than the latest available from the Vault server.
	// +kubebuilder:default=false
	UpdateAvailable bool `json:"updateAvailable"`

	// Message is a human-readable status indication.
	Message string `json:"message,omitempty"`

	// LastTouch is the last time the controller modified the object.
	LastTouch *metav1.Time `json:"lastTouch,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=env

// Environment is the Schema for the environments API
// +kubebuilder:printcolumn:name="Secret Version",type=integer,JSONPath=`.status.version`,description="The version number of the stored secret data"
// +kubebuilder:printcolumn:name="Last Success",type=string,format=date-time,JSONPath=`.status.lastSuccess`,description="The last time data was pulled from Vault"
// +kubebuilder:printcolumn:name="Update Available",type=boolean,JSONPath=`.status.updateAvailable`,priority=10,description="The version in Kubernetes is not the latest available in Vault"
// +kubebuilder:printcolumn:name="Secret Name",type=string,JSONPath=`.spec.secretName`,priority=10,description="The name of the Kubernetes Secret object that this Environment is associated with"
// +kubebuilder:printcolumn:name="Secret Type",type=string,JSONPath=`.spec.secretType`,priority=10,description="The Kubernetes Secret type"
type Environment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   EnvironmentSpec   `json:"spec,omitempty"`
	Status EnvironmentStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// EnvironmentList contains a list of Environment
type EnvironmentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Environment `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Environment{}, &EnvironmentList{})
}
