package updater

import (
	"context"

	"github.com/go-logr/logr"

	vaultApi "github.com/hashicorp/vault/api"

	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Updater is a holding struct for information we need to fetch Vault
// data and turn it into K8s Secret data.
type Updater struct {
	ctx         context.Context
	kubeClient  client.Client
	logger      logr.Logger
	vaultClient *vaultApi.Client
}

func NewUpdater(kubeClient client.Client, logger logr.Logger, vaultClient *vaultApi.Client) *Updater {
	return &Updater{
		ctx:         context.Background(),
		kubeClient:  kubeClient,
		logger:      logger,
		vaultClient: vaultClient,
	}
}
