/*
Copyright 2020 Julie Rostand

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package util

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"net/http"
	"net/url"
	"path"
	"strings"

	vaultApi "github.com/hashicorp/vault/api"

	"github.com/pkg/errors"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	vf1a1 "gitlab.com/jrr/vault-friend/api/v1alpha1"
)

const (
	labelPrefix string = "vault-friend.jrr.io"
)

type VaultSecretData struct {
	Data     map[string]string `json:"data,omitempty"`
	Metadata VaultMetadata     `json:"metadata,omitempty"`
}

type VaultMetadata struct {
	CreatedTime  string `json:"created_time,omitempty"`
	DeletionTime string `json:"deletion_time,omitempty"`
	Destroyed    bool   `json:"destroyed,omitempty"`
	Version      int    `json:"version,omitempty"`
}

func BoolPtr(in bool) *bool {
	return &in
}

func Int32Ptr(in int32) *int32 {
	return &in
}

func DataSecret(typeMeta metav1.TypeMeta, objectMeta metav1.ObjectMeta) *corev1.Secret {
	return &corev1.Secret{
		Type: corev1.SecretTypeOpaque,
		ObjectMeta: metav1.ObjectMeta{
			Namespace:    objectMeta.Namespace,
			GenerateName: "vault-friend-data-",
			Labels: map[string]string{
				PrefixedLabel("parent.kind"): typeMeta.Kind,
				PrefixedLabel("parent.name"): objectMeta.Name,
			},
			OwnerReferences: []metav1.OwnerReference{
				OwnerRef(typeMeta, objectMeta),
			},
		},
	}
}

func ExtractCertificate(input string) (*x509.Certificate, error) {
	block, _ := pem.Decode([]byte(input))

	if block == nil || block.Type != "CERTIFICATE" {
		return nil, errors.New("argument is not a certificate")
	}

	cert, err := x509.ParseCertificate(block.Bytes)

	if err != nil {
		return nil, errors.Wrap(err, "could not parse TLS certificate")
	}

	return cert, nil
}

func GetSupportedOwner(ctx context.Context, kubeClient client.Client, namespace string, ownerRefs []metav1.OwnerReference) (*metav1.OwnerReference, error) {
	if len(ownerRefs) == 0 {
		return nil, errors.New("object is not owned")
	}

	for _, ownerRef := range ownerRefs {
		switch ownerRef.Kind {
		case "CronJob", "DaemonSet", "Deployment", "StatefulSet":
			return &ownerRef, nil
		case "Job":
			job := &batchv1.Job{}
			nsn := NamespacedName(namespace, ownerRef.Name)

			err := kubeClient.Get(ctx, nsn, job)

			if err != nil {
				return nil, errors.Wrap(err, "error trying to find Job owner")
			}

			if len(job.ObjectMeta.OwnerReferences) > 0 {
				for _, ref := range job.ObjectMeta.OwnerReferences {
					if ref.Kind == "CronJob" {
						return &ref, nil
					}
				}
			}
		case "ReplicaSet":
			rs := &appsv1.ReplicaSet{}
			nsn := NamespacedName(namespace, ownerRef.Name)

			err := kubeClient.Get(ctx, nsn, rs)

			if err != nil {
				return nil, errors.Wrap(err, "error trying to find ReplicaSet owner")
			}

			return GetSupportedOwner(ctx, kubeClient, namespace, rs.ObjectMeta.OwnerReferences)
		}
	}

	// No supported owner type
	return nil, nil
}

func KVMetadataPath(path string) string {
	parts := strings.Split(path, "/")

	for i, part := range parts {
		if part == "data" {
			parts[i] = "metadata"
			break
		}
	}

	return strings.Join(parts, "/")
}

func NamespacedName(namespace, name string) types.NamespacedName {
	return types.NamespacedName{
		Namespace: namespace,
		Name:      name,
	}
}

func OwnerRef(typeMeta metav1.TypeMeta, objMeta metav1.ObjectMeta) metav1.OwnerReference {
	return metav1.OwnerReference{
		APIVersion: typeMeta.APIVersion,
		Kind:       typeMeta.Kind,
		Name:       objMeta.Name,
		UID:        objMeta.UID,
		Controller: BoolPtr(true),
	}
}

func PrefixedLabel(label string) string {
	return path.Join(labelPrefix, label)
}

func SecretDeleteAllOfOptions(kind string, objectMeta metav1.ObjectMeta) []client.DeleteAllOfOption {
	return []client.DeleteAllOfOption{
		client.InNamespace(objectMeta.Namespace),
		client.MatchingLabels{
			PrefixedLabel("parent.kind"): kind,
			PrefixedLabel("parent.name"): objectMeta.Name,
		},
	}
}

func SecretListOptions(kind string, objectMeta metav1.ObjectMeta) []client.ListOption {
	return []client.ListOption{
		client.InNamespace(objectMeta.Namespace),
		client.MatchingLabels{
			PrefixedLabel("parent.kind"): kind,
			PrefixedLabel("parent.name"): objectMeta.Name,
		},
	}
}

func ValidateCertificate(input string) error {
	block, _ := pem.Decode([]byte(input))

	if block == nil || block.Type != "CERTIFICATE" {
		return errors.New("argument is not a certificate")
	}

	_, err := x509.ParseCertificate(block.Bytes)

	if err != nil {
		return errors.Wrap(err, "could not parse TLS certificate")
	}

	return nil
}

func VaultClient(vault *vf1a1.Vault) (*vaultApi.Client, error) {
	_, err := url.Parse(vault.Spec.Address)

	if err != nil {
		return nil, errors.Wrap(err, "invalid .spec.address")
	}

	certPool, err := x509.SystemCertPool()

	if err != nil {
		return nil, errors.Wrap(err, "could not get system x509 cert pool")
	}

	if vault.Spec.CACert != "" {
		decodedCert, err := base64.StdEncoding.DecodeString(vault.Spec.CACert)

		if err != nil {
			return nil, errors.Wrap(err, "could not decode .spec.caCert")
		}

		ok := certPool.AppendCertsFromPEM(decodedCert)

		if !ok {
			return nil, errors.New("unable to add .spec.caCert to cert pool")
		}
	}

	tlsConfig := &tls.Config{
		RootCAs: certPool,
	}

	transport := &http.Transport{
		TLSClientConfig: tlsConfig,
	}

	httpClient := &http.Client{
		Transport: transport,
	}

	vaultConfig := &vaultApi.Config{
		Address:    vault.Spec.Address,
		HttpClient: httpClient,
	}

	client, err := vaultApi.NewClient(vaultConfig)

	if err != nil {
		return nil, err
	}

	return client, nil
}
