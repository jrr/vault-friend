package controllers

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	vf1a2 "gitlab.com/jrr/vault-friend/api/v1alpha2"
	util "gitlab.com/jrr/vault-friend/pkg/util"
)

var _ = Describe("DataSource controller", func() {
	const interval = time.Second * 2
	const timeout = time.Second * 10

	Context("When a DataSource is valid", func() {
		It("should reflect the ability to connect to Vault", func() {
			ctx := context.Background()
			objectKey := util.NamespacedName("default", "test-ds-"+randStringRunes(5))

			dataSource := &vf1a2.DataSource{
				ObjectMeta: metav1.ObjectMeta{
					Name:      objectKey.Name,
					Namespace: objectKey.Namespace,
				},
				Spec: vf1a2.DataSourceSpec{
					HostURL: vaultCluster.Cores[0].Client.Address(),
					TLSConfig: &vf1a2.TLSConfig{
						CACert: vaultCluster.CACertPEMFile,
					},
				},
			}

			err := k8sClient.Create(ctx, dataSource)
			Expect(err).NotTo(HaveOccurred(), "failed to create DataSource")

			fetched := &vf1a2.DataSource{}
			Eventually(
				getResourceFunc(ctx, objectKey, fetched),
				timeout, interval).Should(BeNil(), "DataSource should exist")

			// This block exists to ensure the controller has operated on the resource
			Eventually(func() *metav1.Time {
				err = k8sClient.Get(ctx, objectKey, fetched)
				Expect(err).NotTo(HaveOccurred())
				return fetched.Status.LastTouch
			}, timeout, interval).ShouldNot(BeNil(), "controller not operating on DataSource")

			Eventually(func() bool {
				err = k8sClient.Get(ctx, objectKey, fetched)
				Expect(err).NotTo(HaveOccurred())
				return fetched.Status.Connected
			}, timeout, interval).Should(BeTrue(), "DataSource should be able to connect to Vault")
		})
	})

	Context("If the Vault server cannot be reached", func() {
		It("should reflect that in the DataSource .status", func() {
			ctx := context.Background()
			objectKey := util.NamespacedName("default", "test-ds-"+randStringRunes(5))

			dataSource := &vf1a2.DataSource{
				ObjectMeta: metav1.ObjectMeta{
					Name:      objectKey.Name,
					Namespace: objectKey.Namespace,
				},
				Spec: vf1a2.DataSourceSpec{
					HostURL: "http://this-server-does-not-exist",
				},
			}

			err := k8sClient.Create(ctx, dataSource)
			Expect(err).NotTo(HaveOccurred(), "failed to create DataSource")

			fetched := &vf1a2.DataSource{}
			Eventually(
				getResourceFunc(ctx, objectKey, fetched),
				timeout, interval).Should(BeNil(), "DataSource should exist")

			// This block exists to ensure the controller has operated on the resource
			Eventually(func() *metav1.Time {
				err = k8sClient.Get(ctx, objectKey, fetched)
				Expect(err).NotTo(HaveOccurred())
				return fetched.Status.LastTouch
			}, timeout, interval).ShouldNot(BeNil(), "controller not operating on DataSource")

			Eventually(func() bool {
				err = k8sClient.Get(ctx, objectKey, fetched)
				Expect(err).NotTo(HaveOccurred())
				return fetched.Status.Connected
			}, timeout, interval).Should(BeFalse(), "DataSource should not be able to connect to Vault")
		})
	})
})
