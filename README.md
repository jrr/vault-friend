# Vault-Friend

Everybody can use a friend sometimes. If you're
trying to get secrets from [Vault](https://vaultproject.io)
into containers in [Kubernetes](https://kubernetes.io),
this is one way to do it.

## How it works

**Note:** For info on Vault-Friend before 0.3, see
[the old README](README.old.md). The `v1alpha1` API is
**deprecated** and will be removed in release v0.5.

Vault-Friend v1alpha2 supports three CRDs (for now):

- ClusterDataSource
- DataSource
- Environment

(Cluster)DataSource is your Vault connection. The
difference is availability: a DataSource is
namespaced, where a ClusterDataSource is not.

Environment will set you up with a generic
Kubernetes secret ideal for `envFrom.secretRef` use.
It syncs data from Vault on a standard 15-minute
interval (configurable as `.spec.checkInterval`) or
you can pin a particular secret version with
`.spec.pinVersion`.

Vault-Friend is designed to be easy. You authorize
your containers in Vault with the regular Kubernetes
auth engine, mounted anywhere you like. The only
additional Vault policy required is a metadata
reader. Vault-Friend leverages its access to Kubernetes
secrets in order to use your application's token for
communicating with Vault.

### Update checking logic

*AKA "the section I really should have added sooner in retrospect"*

Vault-Friend steps through the following logic to determine if
a secret should be sync'd from its (Cluster)DataSource:

- Fetch the latest secret metadata from Vault, extracting `version`
- IF `pinVersion` is non-zero AND the metadata version doesn't match the `pinVersion`, UPDATE
- ELSE IF `pinVersion` is `0` (default):
    - IF `alwaysUpdate` is `true`, UPDATE
    - ELSE IF `alwaysUpdate` is `false` (default) AND the metadata version doesn't match the stored Kubernetes Secret's version, UPDATE

## Requirements for operation

Vault-Friend is light on requirements, but it does
have a few. If you meet them, you should be able to
very quickly deploy this operator with kustomize.

- Your Vault KV engines must be v2. Sorry, no v1
  support (yet?).
- You must be using the Kubernetes auth engine to
  authenticate and authorize your pods with Vault.
- [Cert-manager](https://cert-manager.io) is
  strongly recommended, and the default Kustomize
  manifests will assume that you are using it.

## Installation

1. Set up a policy in Vault that grants `list` and `read` access
to metadata (important note: this means paths like
`secret/metadata/something`) for all secrets engines you wish to use.

2. Assign this policy to a role in Vault's Kubernetes auth engine.

3. With kustomize, apply the manifests in `config/default`.

4. You're now ready to create your first DataSource and Environment.

## Example objects

Let's take a look at the two major types of CRDs,
first DataSource (recall the *only* difference
between DataSource and ClusterDataSource is that
the latter is not namespaced).

```yaml
apiVersion: vault-friend.jrr.io/v1alpha2
kind: DataSource
metadata:
  name: example-source
  namespace: vf-example
spec:

  # This should be a Vault server address as from VAULT_ADDR.
  # required
  hostURL: "https://vault-server.company.co"

  # This is the Kubernetes auth engine role that Vault-Friend will
  # use to fetch metadata for e.g., update checking.
  # The default is "vault-friend".
  metadataRole: volt-frond

  # If you have more than one Kubernetes cluster tied to your
  # Vault, or just particular opinions about naming things, this will
  # tell Vault-Friend where to find your K8s auth engine.
  # The default is "kubernetes" as that's also the Vault default.
  authEngineMountPath: kivernitis

  # If you need to configure TLS settings for your Vault server,
  # this is where you'd do that. It's a mirror of the same
  # object from the Vault API.
  tlsConfig:

    # CACert is the path to a PEM-encoded CA cert file to use to verify the
    # Vault server SSL certificate.
    caCert: /path/to/file.pem

    # CAPath is the path to a directory of PEM-encoded CA cert files to verify
    # the Vault server SSL certificate.
    caPath: /path/to/ca-certs

    # ClientCert is the path to the certificate for Vault communication.
    clientCert: /path/to/client.pem

    # ClientKey is the path to the private key for Vault communication.
    clientKey: /path/to/client.key

    # TLSServerName, if set, is used to set the SNI host when connecting via TLS.
    tlsServerName: vault-server.internal

    # Insecure enables or disables SSL verification.
    insecure: false
```

Now let's look at an Environment.

```yaml
apiVersion: vault-friend.jrr.io/v1alpha2
kind: Environment
metadata:
  name: some-app-idk
  namespace: vf-example
spec:

  # secretName is where your secret data will be stored.
  # required
  secretName: some-app-env-idk

  # dataSourceRef is as it sounds, a reference to your (Cluster)DataSource
  # required
  dataSourceRef:

    # kind will be either ClusterDataSource or DataSource.
    # required
    kind: DataSource

    # name is the .metadata.name of the data source you
    # want to use.
    # required
    name: example-source

    # authRole is the Vault K8s auth engine role that
    # Vault-Friend will use to sync data.
    # required
    authRole: some-app-idk

  # serviceAccountName tells Vault-Friend which K8s
  # ServiceAccount to use the token of for syncing data.
  # Typically, this will be the app's SA.
  # required
  serviceAccountName: some-app-idk

  # sourcePath is the path in Vault where the secret
  # data should be found.
  # required
  sourcePath:

    # kv provides a KV v2 engine **data** path for secret data.
    # At the moment, only kv is supported, thus it's required.
    # required
    kv: secret/data/vf-example/some-app-idk

  # secretType allows you to set the type of the Secret that will
  # be stored in K8s.
  # The default is "Opaque".
  secretType: kubernetes.io/basic-auth

  # alwaysUpdate will force Vault-Friend to sync your secret
  # data on every checkInterval. No effect if pinVersion is
  # non-zero.
  # The default is false.
  alwaysUpdate: false

  # pinVersion will lock your data to a specific version in Vault.
  # It will override alwaysUpdate if the latter is set to true.
  # The default is 0 (== do not pin version)
  pinVersion: 3

  # keyPrefix allows you to add a prefix to all keys. This will
  # cause your Kubernetes secret to differ from its Vault version,
  # but as long as the metadata version is the same, there won't
  # be any spurious syncing.
  # Occurs *before* the capitalizeKeys step.
  # optional
  keyPrefix: app_env_

  # capitalizeKeys will uppercase all possible characters in the
  # secret data keys. Typical of UNIX_ENV_VAR_STYLE. This transform
  # does not affect update check results.
  # Occurs *after* the keyPrefix step.
  # The default is false.
  capitalizeKeys: true

  # checkInterval is how often Vault-Friend will re-examine your
  # Environment resource to determine if there is an update available
  # and if desired, act on it. This should be a string representation
  # of a period of time, e.g., "30s" for 30 seconds. The largest
  # available unit is "h" - hours, a limitation of Golang's
  # time.ParseDuration() function.
  # The default is "15m" (15 minutes).
  checkInterval: 30m
```

## Helpful tips

- If you get bored waiting for your broken Environment to hit another
backoff interval so you can test the external change you made, consider
instead using the `vault-friend.jrr.io/force-refresh` annotation! You
can set it to any value and it will trigger a reconciliation cycle, in
the process deleting itself so as not to cause loops.
